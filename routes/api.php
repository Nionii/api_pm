<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('biodata','BiodataController@index');
Route::post('biodata','BiodataController@create');
Route::get('/profile/{id}','BiodataController@profile');
Route::put('/biodata/{id}','BiodataController@update');
Route::delete('/biodata/{id}','BiodataController@delete');