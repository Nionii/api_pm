<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $fillable = ['id', 'nama_lengkap', 'username','inisial', 'email','bio','foto'];

    public function users(){
    	return $this->hasOne(User::class);
    }
}
