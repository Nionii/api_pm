<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biodata;

class BiodataController extends Controller
{
    //

    public function index(Biodata $biodata){
    	$biodata = Biodata::all();
    	// $data = ['bio'=>$biodata];
    	// return $data;

        return response()->json([
            'pesan' => 'berhasil',
            'bio' => $biodata] , 200);

    }

    public function create(Request $request){
    	$biodata = new Biodata();
    	$biodata->nama = $request->nama;
    	$biodata->foto = $request->foto;
    	$biodata->save();

    	return $biodata;
    }

    public function update(Request $request, $id){
    	$biodata = Biodata::find($id);
    	$biodata->nama = $request->nama;
    	$biodata->foto = $request->foto;
    	$biodata->save();

    	return "Data Terupdate";
    }

    public function delete($id){
		$biodata = Biodata::find($id);
		$biodata->delete();

		return "Data terhapus";
    }

    public function profile($id){
    	$biodata = Biodata::find($id);
    	return $biodata;
    }
}
